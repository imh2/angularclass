//crear controller y vincularlo a la aplicacion

//aqui asumimos que el modulo ya esta creado
angular.module("AngularClass").controller("MainController", function($scope, $routeParams)
{
    //two way databinding: ahora el html modificará el modelo

    function init()
    {
        $scope.name = "";
        $scope.todoList = [];
        $scope.saludo = "hola mundo angular";
        $scope.message = "";
    }
    init();

    $scope.isPikachu = function()
    {
        return $scope.name.indexOf('pikachu') != -1;

    }

    $scope.addTodo = function()
    {
        $scope.todoList.push($scope.todo);
        $scope.todo="";
    }

    $scope.removeTodo = function(index)
    {
        $scope.todoList.splice(index, 1);
    }
});

angular.module("AngularClass").controller("view1Controller", function($scope){
    console.log("into viewcontroller1");
    $scope.saludo = "Hemos cambiado la ruta a la primera vista";
})